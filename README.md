# Introdución a Swift


El curso esta desglosado en proyectos independientes en los que progresivamente se
muestran  de una forma práctica los nuevas caracteristicas del lenguaje 
(programación orientada a protocolos, opcionales, programacion funcional). Durante el
curso se indicará al como acceder a recursos avanazados para consolidar los conceptos 
vistos en la formación.

 Además de las nuevas caracteristicas que introduce Swift se explicaran las buenas 
 practicas para gestionar un proyecto (gestión de versiones con Git, TDD, automatización
 de de los procesos de compilación y subida a la AppStore, uso de herramientas de 
 monitorización)


___ 
**Dia 1.**   
```dia1
    AutoLayout + NSStackView
    IBOutlet, IBAction, Segues  
    Introducción a la sintaxis de Swift
    Optionals, unwraping optionals, Optional binding, multiple optional binding
    
```       
       
       
___      
**Dia 2**.  
```dia2  
       UIKit
       UIButton, UILabel, UISlider, UIAlert  
       Dependency injection to pass information between ViewControllers 
       NSUSerDefaults
```
___
**Dia 3**.
```dia3
       iOS Networking
       Networking and Web services
       Parsing JSON
       Networking with MVC
       ATS App Transport Security
       Perfom Asyncronous operation on iOS (Grand Central Dispatch)
       Authentication, Authorization
       Network reliability
```
___      
**Dia 4**. 
```dia4
       Closures
       Grand Central Dispatch y tareas asincronas
       
```
___  
**Dia 5**. 
```dia5
 NSCoder/NSKeyedArchiver
 Persistent Objects & Core Data
 Core Data
```

  



___



  
  



## Student Resources:

**Swift Education**
http://swifteducation.github.io  

**Libro Swift Apple**  
https://itunes.apple.com/us/book/the-swift-programming-language/id881256329?mt=11  

** Start Learning Swift **
https://developer.apple.com/library/ios/referencelibrary/GettingStarted/DevelopiOSAppsSwift/index.html?utm_source=statuscode&utm_medium=email



Durante el curso se analizaran herramientas de terceros utilizadas de forma habitual
para crear aplicaciones:  
**Postman**   
https://chrome.google.com/webstore/detail/postman-rest-client-short  
**Sketch**  
http://www.sketchapp.com   
**MitmProxy** 
https://mitmproxy.org  
**PaintCode**
http://www.paintcodeapp.com  







## Requisitos 
El curso asume que el alumno tiene experiencia previa con algun lenguaje de 
programación. Especificamente debe conocer los conceptos de variables, sentencias,
loops, funciones y clases.
 El alumno dispondra de equipos Mac con la ultima versión de XCode. Al principio
 del curso se ejecutará el primer proyecto para validar el software y hardware disponible.



